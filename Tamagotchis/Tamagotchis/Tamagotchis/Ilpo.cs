﻿using System;

namespace Tamagotchis
{
    public class Dude : Tamagotchi
    {
        public Dude(string nimi) : base(nimi)
        {
            happinessIndex = 15;
        }
        public override string ToString()
        {
            return GetType().Name + ":" + name + ", reagointitahti " + pace + ", onnellisuusindeksi " + happinessIndex;
        }
    }
}
