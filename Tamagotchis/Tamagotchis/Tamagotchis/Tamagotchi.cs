﻿using Pets;

namespace Tamagotchis
{
    abstract public class Tamagotchi
    {
        protected int happinessIndex;
        public readonly string name;
        protected uint pace;

        protected Tamagotchi(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new System.ArgumentException("message", nameof(name));
            }

            this.name = name;
        }

        public uint Pace { get => pace; set => pace = value; }
        public int Happiness
        {
            get => happinessIndex;
            set
            {
                if (value > 0)
                {
                    happinessIndex = value;
                }
                else
                {
                    happinessIndex = 0;
                }
            }
        }

        virtual public bool Act(uint count)
        {
            bool result = false;
            if (count % pace == 0)
            {
                Eat();
                Sleep();
                Play();                
                result = true;
            }
            return result;
        }
        public void Eat()
        {
           Happiness += 1;
        }

        public void Sleep()
        {
            Happiness -= 10;
        }

        public void Medicate()
        {
            Happiness -= 3;
        }

        public void Pee()
        {
            Happiness -= 2;
        }

        public void Read()
        {
            Happiness += 5;
        }

        public void Play()
        {
            
        }

    }
}
