﻿namespace Tamagotchis
{
    public class Dino : Tamagotchi
    {
        public Dino(string nimi):base(nimi)
        {
            happinessIndex = 10;
        }
        public override string ToString()
        {
            return GetType().Name + ":" + name + ", reagointitahti " + pace + ", onnellisuusindeksi " + happinessIndex;
        }
    }
}
