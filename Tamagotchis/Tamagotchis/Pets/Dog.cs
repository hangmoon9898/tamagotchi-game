﻿using System;

namespace Pets
{
    public class Dog : IPet
    {
        public readonly string name;
        public readonly string breed;

        public Dog(string name, string breed)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.breed = breed ?? throw new ArgumentNullException(nameof(breed));
        }

        public string Eat()
        {
            return($"{GetType().Name}, {name},   eats");
        }

        public string Sleep()
        {
            return($"{GetType().Name}, {name},   sleeps");
        }
        public string Bark()
        {
            return($"{GetType().Name}, {name},   barks");
        }

        public string Run()
        {
            return($"{GetType().Name}, {name},   runs");
        }
    }
}
