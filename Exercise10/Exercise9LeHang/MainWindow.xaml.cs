﻿using System;
using System.IO;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Tamagotchis;
using System.Windows.Controls;

namespace Exercise9LeHang
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Dino dude = new Dino("Baby");
        private DispatcherTimer dispatcherTimer;
        private int tickInMillis = 1000; //thread operation interval
        private uint count = 1000;
        Dodgeball dodgeball = new Dodgeball();
        private string fileName = Directory.GetCurrentDirectory() + @"\Resources\stats.dat";


        public MainWindow()
        {
            InitializeComponent();          
            Init();
            UpdateTxt();
           
        }

        private void Init()
        {
            //timer's initial values
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(Tick_Action);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, tickInMillis);
            dispatcherTimer.Start();
            dude.Pace = 10;
            Deserialize();
        }

        private void UpdateTxt()
        {
            if (dude.Happiness <= 0)
            {
                MessageBox.Show("Tamagotchi died");
                Tamagotchi1.Visibility = Visibility.Collapsed;
                Eat.IsEnabled = false;
                Sleep.IsEnabled = false;
                Play.IsEnabled = false;
                Read.IsEnabled = false;
                Medicate.IsEnabled = false;
                Pee.IsEnabled = false;
                dispatcherTimer.Stop();
            }
            tama.Text = dude.Happiness.ToString();

        }

        /// <summary>
        /// serialize
        /// </summary>
        public void Serialize()
        {
            Stream FileStream = null;
            try
            {
                FileStream = File.Create(fileName);
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(FileStream, dude.Happiness);
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("Serialization failed");
            }
            finally
            {
                if (FileStream != null)
                {
                    FileStream.Close();
                }
            }
        }
        /// <summary>
        /// deserialize
        /// </summary>
        /// <returns></returns>
        public bool Deserialize()
        {
            bool result = false;
            Stream FileStream = null;
            try
            {
                if (File.Exists(fileName))
                {
                    FileStream = File.OpenRead(fileName);
                    BinaryFormatter deserializer = new BinaryFormatter();
                    object o = deserializer.Deserialize(FileStream);
                    if (o != null)
                    {
                        dude.Happiness = Convert.ToInt32(o);
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("Deserialization failed");

            }
            finally
            {
                if (FileStream != null)
                {
                    FileStream.Close();
                }
            }
            return result;
        }


        private void Tick_Action(object sender, EventArgs e)
        {
            if (dispatcherTimer != null)
            {
                if (count >= 0)
                {
                    if (count-- % dude.Pace == 0) // Eating Pace = 10
                    {
                        MessageBox.Show("Feed me");
                    }
                   else  
                   if (count-- % (dude.Pace*2 + 6) ==  0) //Sleeping Pace = 26
                   {
                        MessageBox.Show("I need to sleep");
                   }
                   else
                   if (count-- % (dude.Pace + 3) == 0) //Peeing Pace = 13
                    {
                        MessageBox.Show("I have to pee");
                    }

                    else 
                    if (count-- % (dude.Pace * 4 + 2) == 0) //Sick Pace = 42;
                    {
                        MessageBox.Show("I am sick :(");
                    }

                }
                else
                {
                    count = 1000;
                }
            }
        }

        //Eating increases 1 Happiness Index (HI)
        private void Eat_Click(object sender, RoutedEventArgs e)
        {

            dude.Eat();
            UpdateTxt();
        }

        //Sleeping decreases 10 HI
        private void Sleep_Click(object sender, RoutedEventArgs e)
        {            
            dude.Sleep();
            UpdateTxt();
        }

        //Medicate decreases 3 HI
        private void Medicate_Click(object sender, RoutedEventArgs e)
        {
            dude.Medicate();
            UpdateTxt();
        }

        //Pee decreases 2 HI
        private void Pee_Click(object sender, RoutedEventArgs e)
        {
            dude.Pee();
            UpdateTxt();
        }

        //Read increases 5 HI
        private void Read_Click(object sender, RoutedEventArgs e)
        {
            dude.Read();
            MessageBox.Show("Thanks, I like being a smart dino");
            UpdateTxt();
        }

        private void Play_Click(object sender, RoutedEventArgs e)
        {
            dodgeball.Picture.Source = (((sender as Button).Content as DockPanel).Children[0] as Image).Source;
            dodgeball.ShowDialog();
            dude.Happiness += dodgeball.Happiness;
            UpdateTxt();
        }

        void Window_Closing(object sender, CancelEventArgs e)
        {
            Serialize();
        }
    }
}
