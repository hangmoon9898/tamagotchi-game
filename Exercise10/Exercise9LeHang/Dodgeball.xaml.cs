﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Games;
using Tamagotchis;

namespace Exercise9LeHang
{
    /// <summary>
    /// Interaction logic for Dodgeball.xaml
    /// </summary>
    public partial class Dodgeball : Window
    {
        public Dino baby = new Dino("Baby");

        private DispatcherTimer dispatcherTimer;
        private int millis = 2000;

        private double[] X = { 100, 300, 500, 100, 300, 500, 100, 300, 500 };
        private double[] Y = { 100, 100, 100, 200, 200, 200, 300, 300, 300 };
        private int square; //tamagotchi location
        private Games.Game game = new Games.Game();

        private DependencyProperty pointProperty = DependencyProperty.Register(
           "Points", typeof(int), typeof(Window));
        public int Points
        {
            get { return (int)GetValue(pointProperty); }
            set { SetValue(pointProperty, value); }
        }
        private DependencyProperty happinessProperty = DependencyProperty.Register(
           "Happiness", typeof(int), typeof(Window));

        private DependencyProperty missesProperty = DependencyProperty.Register(
           "Misses", typeof(byte), typeof(Window));
        public byte Misses
        {
            get { return (byte)GetValue(missesProperty); }
            set { SetValue(missesProperty, value); }
        }

        public int Happiness
        {
            get { return (int)GetValue(happinessProperty); }
            set { SetValue(happinessProperty, value); }
        }

        public Dodgeball()
        {
            InitializeComponent();
            Init();
            DataContext = this;
            //DispatcherTimer settings and Dispatcher start
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            //adding event
            dispatcherTimer.Tick += new EventHandler(DispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, millis);
            dispatcherTimer.Start();

        }

        private void Init()
        {
            millis = 2000;
            Points = 0;
            Misses = 0;
            Happiness = 0;
            game.Init();
            ChangePicture();
        }
        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            ChangePicture();
            CommandManager.InvalidateRequerySuggested();
        }

        private void ChangePicture()
        {
            square = game.Move(true) - 1;
            //set location for picture on MyCanvas, picture source
            //set in xaml code
            Canvas.SetLeft(Picture, X[square]);
            Canvas.SetTop(Picture, Y[square]);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        private void MyCanvas_MouseDown(object sender,
            MouseButtonEventArgs e)
        {
            //mouse location when event fired
            double x = e.GetPosition(MyCanvas).X;
            double y = e.GetPosition(MyCanvas).Y;
            //Game knows squares 1..9
            int hit = 1;
            if (x >= 100 && x <= 200 && y >= 100 && y < 200) hit = 1;
            else if (x >= 300 && x <= 400 && y >= 100 && y < 200) hit = 2;
            else if (x >= 500 && y <= 600 && y >= 100 && y < 200) hit = 3;
            else if (x >= 100 && x <= 200 && y >= 200 && y < 300) hit = 4;
            else if (x >= 300 && x <= 400 && y >= 200 && y < 300) hit = 5;
            else if (x >= 500 && y <= 600 && y >= 200 && y < 300) hit = 6;
            else if (x >= 100 && x <= 200 && y >= 300 && y < 400) hit = 7;
            else if (x >= 300 && x <= 400 && y >= 300 && y < 400) hit = 8;
            else if (x >= 500 && y <= 600 && y >= 300 && y < 400) hit = 9;
            if (game.IsHit(hit))
            {
                Points = (int)game.Points;
                if (Points % 10 == 0)
                {
                    Happiness += 1;
                }
                millis = (Points % 10 == 0) ? millis - 100 : millis;
                ChangePicture();
            }
            else
            {
                Misses++;
                if (game.IsReady())
                {
                    dispatcherTimer.Stop();
                    Happiness -= 10;
                    if (MessageBoxResult.Yes == MessageBox.Show("Do you want to play again?", "Third miss!", MessageBoxButton.YesNo))
                    {                        
                        if (Happiness != 0)
                        {
                            int temp;
                            temp = Happiness;
                            Init();
                            Happiness = temp;
                        }
                        else
                        {
                            Init();
                        }

                        dispatcherTimer.Start();
                    }
                    else
                    {
                        Close();                     
                    }
                }
            }
        }
    }
}
